# English models for Natural Language Understanding

Pre-trained models for natural language understanding of english.

## Installation

> You need `git-lfs`, `rasa` and `rasa-sdk` to download and serve this model.

Use the [`Domain Management Tool`](https://gitlab.com/waser-technologies/technologies/dmt) to automaticaly download and serve this model.

```zsh
dmt --serve --lang en
```

Or you can do it manualy.

```
git clone https://gitlab.com/waser-technologies/models/en/nlu.git ./en-nlu
cd ./en-nlu
rasa shell -m models/ --enable-api --endpoints ./endpoints.yml --credentials ./credentials.yml
```

## What is this model capable of?

From chitchat to complex web queries, this model aims to be the one to rule them all.

This models was trained using the following domains:

-   [SmallTalk](https://gitlab.com/waser-technologies/data/nlu/en/smalltalk)
-   [Web Search](https://gitlab.com/waser-technologies/data/nlu/en/web-search)

